import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Мөстәкыйль',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Selimcan.org-тагы дәреслекләр башлыча өйдә, мөстәкыйль рәвештә белем
        алучыларга тәгаенләнгән.
      </>
    ),
  },
  {
    title: 'Эффектив',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        Темалар җиңелдән кыенга таба логик эзлеклелектә бирелә; төп игътибар
          биш һөнәргә юнәлтелгән — исәпләү һәм исбатлау (математика), язу,
	  уку, рәсем сызу һәм компьютер
        программаларын инша итү. Ягъни дәрес төрләре саны да һичбер заман 4-5-тән
        артык түгел.
      </>
    ),
  },
  {
    title: 'Күптелле',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Максат — хәлдән килгәнчә күп тел өчен иң мөһим дәреслекләр һәм
        әдәбият җыентыгын булдыру.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
